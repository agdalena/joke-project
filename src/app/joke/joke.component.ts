import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Joke } from '../joke';

@Component({
  selector: 'app-joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.scss']
})
export class JokeComponent {

  @Input()
   data: Joke;

  @Output()
  jokeRemoved = new EventEmitter<Joke>();

  delete(joke) {
    this.jokeRemoved.emit(joke);
  }



}
